# Use-Case Raumklima-Monitoring

## Projektbeschreibung

In diesem Use-Case wurde die Integration des "Tektelic Vivid Smart Room Sensor" durchgeführt. Der Sensor misst die relative Luftfeuchtigkeit und Temperatur in einem Raum.

Der Use-Case wurde auf der FIWARE-basierten [Urban Dataspace Platform](https://gitlab.com/urban-dataspace-platform/core-platform) entwickelt und integriert.

## Installationsanleitung

Um den Anwendungsfall zu installieren sind folgende Schritte nötig:  
1. Eine lokale Maschine für die Installation ist vorzubereiten (Siehe https://gitlab.com/urban-dataspace-platform/core-platform/-/blob/master/00_documents/Admin-guides/cluster.md).
2. Eine NodeRED-Instanz deployen oder eine vorhandene Instanz verwenden:
    - Deployment analog zu den *demo_usecases*: https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/deployment
3. *flow.json* in der NodeRED-Instanz deployen.

## Gebrauchsanweisung

Der NodeRED-Flow ruft die Daten per HTTP-Request von der ElementIOT-API ab. Dabei kann eine Folder-ID angegeben werden, um alle Devices aus einem Ordner mit einem Request abzufragen. Für die ElementIOT-API ist ein API-Key erforderlich.

Diese Daten werden in das NGSI-kompatible [AirQualityObserved Smart-Data-Model](https://github.com/smart-data-models/dataModel.Environment/blob/master/AirQualityObserved/README.md) umgewandelt und an die Datenplattform gesendet:

```json
{
   "id":"urn:ngsi-ld:AirQualityObserved:2026f0b6-776d-476f-8121-000751928715",
   "type":"AirQualityObserved",
   "name":{
      "type":"Text",
      "value":"Tektelic Smart Room 2025A0051_RTW3"
   },
   "alternateName":{
      "type":"Text",
      "value":"tektelic-smart-room-2025a0051"
   },
   "dateObserved":{
      "type":"DateTime",
      "value":"2024-05-03T12:32:17.340Z"
   },
   "relativeHumidity":{
      "type":"Number",
      "value":90
   },
   "temperature":{
      "type":"Number",
      "value":5.1
   }
}
```

## Kontaktinformationen

Dieses Projekt wurde von der [Hypertegrity AG](https://www.hypertegrity.de/) entwickelt.

## Lizenz

Dieses Projekt wird unter der EUPL 1.2 veröffentlicht.

## Link zum Original-Repository

https://gitlab.com/urban-dataspace-platform/use_cases/integration/raumklima_monitoring/nr-tektelic-smart-room